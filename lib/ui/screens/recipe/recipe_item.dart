import 'package:flutter/material.dart';
import 'package:flutter_app_final/assets/theme/app_colors.dart';
import 'package:flutter_app_final/ui/screens/recipe_detail/recipe_detail_screen.dart';

class RecipeItem extends StatelessWidget {
  final dynamic item;

  const RecipeItem({Key key, @required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => RecipeDetailScreen(
                    image: item.recipe.image,
                    ingredients: item.recipe.ingredientLines)),
          );
        },
        child: Container(
          height: 100.0,
          child: Row(children: <Widget>[
            Expanded(
                child: SizedBox(
                  width: 150,
                  child: Hero(
                    tag: item.recipe.image,
                    child: Image.network(item.recipe.image, fit: BoxFit.fill,),
                  )
                )),
            Padding(
                padding: const EdgeInsets.only(top: 4, left: 5, right: 5),
                child: SizedBox(
                  width: 150,
                  child: Text(
                    item.recipe.label,
                    style: new TextStyle(fontSize: 15.0, color: Colors.white,),
                  ),
                )),
            Padding(
              padding: const EdgeInsets.only(bottom: 16),
              child: SizedBox(
                width: 150,
                child: Text(item.recipe.calories.toString(),
                    style: new TextStyle(fontSize: 12.0, color: AppColors.getColor(COLOR_LIGHT_GRAY),)),
              ),
            )
          ]),
        ));
  }
}
