import 'package:flutter/material.dart';
import 'package:flutter_app_final/bloc/recipe/recipe_bloc.dart';
import 'package:flutter_app_final/ui/screens/base_widget.dart';
import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_app_final/assets/theme/app_colors.dart';
import 'package:flutter_app_final/bloc/recipe/recipe_event.dart';
import 'package:flutter_app_final/bloc/recipe/recipe_state.dart';
import 'package:flutter_app_final/data/model/recipe_model.dart';
import 'package:flutter_app_final/ui/screens/history/history_screen.dart';
import 'package:flutter_app_final/ui/screens/recipe/recipe_item.dart';
import 'package:flutter_app_final/utils/custom_popup_menu.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:package_info/package_info.dart';

class RecipeScreen extends BaseWidget<RecipeBloc>{

  @override
  RecipeBloc getScreenBloc() => RecipeBloc();

  @override
  Widget getWidget(BuildContext context) => RecipeWidget();

}

class RecipeWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _RecipeWidgetState();
}

class _RecipeWidgetState extends State<RecipeWidget> with TickerProviderStateMixin {
  RecipeBloc get _bloc => BlocProvider.of<RecipeBloc>(context);

  Widget _searchButton = Icon(Icons.search, color: Colors.white);
  Widget _saveButton = Icon(Icons.save, color: Colors.white);
  Widget _sortButton = Icon(Icons.sort, color: Colors.white);
  Widget _appBarTitle = Text("Flutter final", style: TextStyle(color: Colors.white));
  TextEditingController _searchQuery =  TextEditingController();
  bool _isClicked = false;
  bool _isNeedSortedFromMinToMax = true;
  bool _isNetworkConnected = true;
  List<Hit> _listHit = List();
  StreamSubscription<ConnectivityResult> _subscription;
  List<CustomPopupMenu> _menu = <CustomPopupMenu>[
    CustomPopupMenu(title: describeEnum(MenuTitle.History), icon: Icons.history),
    CustomPopupMenu(title: describeEnum(MenuTitle.About), icon: Icons.error)
  ];
  AnimationController _controller;
  CurvedAnimation _curve;
  String _version;
  int textCount = 0;

  @override
  void initState() {
    searchListener();
    connectivityListener();
    _controller = AnimationController(duration: const Duration(milliseconds: 2000), vsync: this);
    _curve = CurvedAnimation(parent: _controller, curve: Curves.easeIn);

    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      _version = packageInfo.version;
    });

    super.initState();
  }

  void searchListener() {
    _searchQuery.addListener(() {
      if (_searchQuery.text.isNotEmpty && textCount != _searchQuery.text.length) {
        textCount = _searchQuery.text.length;
        Future.delayed(Duration(milliseconds: 500), () {
          _bloc.dispatch(RecipeActionEvent(_searchQuery.text));
        });
      }
    });
  }

  void connectivityListener() {
    _subscription = Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      if (result == ConnectivityResult.mobile || result == ConnectivityResult.wifi){
        setState(() {
          _isNetworkConnected = true;
        });
      } else {
        setState(() {
          _showDialog();
          _isNetworkConnected = false;
        });
      }
    });
  }

  @override
  void dispose() {
    _bloc.dispose();
    _subscription.cancel();
    _searchQuery.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: _buildAppBar(), body: _buildBody());
  }

  AppBar _buildAppBar() {
    return AppBar(
        centerTitle: true,
        title: _appBarTitle,
        automaticallyImplyLeading: false,
        actions: _isNetworkConnected ? <Widget>[
          IconButton(icon: _searchButton, onPressed: () {setState(() {_startOrStopSearch();});},),
          IconButton(icon: _sortButton, onPressed: () {setState(() {_sortItems();});},),
          IconButton(icon: _saveButton, onPressed: () {setState(() {_saveToFireBase();});}),
          _popupMenu()
        ] : <Widget>[
          IconButton(icon: _searchButton, onPressed: () {setState(() {_startOrStopSearch();});},),
          IconButton(icon: _sortButton, onPressed: () {setState(() {_sortItems();});},),
          _popupMenu()]
    );
  }

  PopupMenuButton<CustomPopupMenu> _popupMenu() {
    return PopupMenuButton<CustomPopupMenu>(
      elevation: 3,
      onSelected: _select,
      itemBuilder:  (BuildContext context) {
        return _menu.map((CustomPopupMenu choice) {
          return PopupMenuItem<CustomPopupMenu>(
            value: choice,
            child: Text(choice.title.toString()),
          );
        }).toList();
      },
    );
  }

  void _startOrStopSearch() {
    if (!_isClicked) {
      _searchButton = Icon(Icons.close, color: Colors.white,);
      _appBarTitle = TextField(
          controller: _searchQuery,
          style: TextStyle(color: Colors.white),
          decoration: InputDecoration(
              prefixIcon: Icon(Icons.search, color: Colors.white),
              hintText: "Search...",
              hintStyle: TextStyle(color: Colors.white)));

      _isClicked = true;
    } else {
      _searchButton = Icon(Icons.search, color: Colors.white,);
      _appBarTitle = Text("Flutter final", style: new TextStyle(color: Colors.white));
      _searchQuery.clear();

      _isClicked = false;
    }
  }

  Container _buildBody() {
    return Container(
      child: Stack(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.only(top: 10, bottom: 10),
            color: AppColors.getColor(COLOR_BACKGROUND),
            child: BlocBuilder<RecipeBloc, RecipeState>(
              builder: (context, state) {
                if (state is InitialRecipeState) {
                  return Container();
                }
                if (state is LoadingRecipeState) {
                  return Center(child: CircularProgressIndicator());
                }
                if (state is ErrorRecipeState) {
                  return Center(child: Text(state.message));
                }
                if (state is ResultRecipeState) {
                  _listHit = state.response;
                  return ListView.builder(
                      itemCount: _listHit.length,
                      itemBuilder: (context, index){
                        final item = _listHit[index];

                        return Dismissible(
                          key: Key(item.recipe.calories.toString()),
                          onDismissed: (direction) {
                            setState(() {
                              _listHit.removeAt(index);
                            });
                          },
                          background: Container(color: AppColors.getColor(COLOR_MAIN_10)),
                          child: RecipeItem(item: _listHit[index]),
                        );
                      });
                }
                return Text('');
              },
            ),
          ),
          Align (
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 20),
                child: FadeTransition(
                    opacity: _curve,
                    child: Text("App version: " + _version, style: new TextStyle(color: Colors.white))),
              )
          ),
        ],
      ),
    );
  }

  void _saveToFireBase() {
    _bloc.clearCloud(_listHit);
  }

  void _sortItems() {
    setState(() {
      if (_isNeedSortedFromMinToMax){
        _listHit.sort((a, b) => a.recipe.calories.compareTo(b.recipe.calories));
        _isNeedSortedFromMinToMax = false;
      } else {
        _listHit.sort((b, a) => a.recipe.calories.compareTo(b.recipe.calories));
        _isNeedSortedFromMinToMax = true;
      }
    });
  }

  Future<void> _showDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
            title: Text('No internet connection')
        );
      },
    );
  }

  void _select(CustomPopupMenu choice) {
    setState(() {
      if (choice.title == describeEnum(MenuTitle.History)) {
        Navigator.push(context, MaterialPageRoute(builder: (context) => HistoryScreen()));
      } else {
        _controller.forward();
        Timer(const Duration(milliseconds: 5000), () {
          _controller.reverse();
        });
      }
    });
  }
}
