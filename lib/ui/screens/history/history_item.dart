import 'package:flutter/material.dart';
import 'package:flutter_app_final/assets/theme/app_colors.dart';
import 'package:flutter_app_final/data/model/firebase_model.dart';
import 'package:flutter_app_final/ui/screens/recipe_detail/recipe_detail_screen.dart';

class HistoryItem extends StatelessWidget {
  final Data item;

  const HistoryItem({Key key, @required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        child: Container(
          height: 100.0,
          child: Row(children: <Widget>[
            Expanded(
                child: SizedBox(
                  width: 150,
                  child: Hero(
                    tag: item.image,
                    child: Image.network(item.image, fit: BoxFit.fill,),
                  )
                )),
            Padding(
                padding: const EdgeInsets.only(top: 4, left: 10),
                child: SizedBox(
                  width: 150,
                  child: Text(
                    item.label,
                    style: new TextStyle(fontSize: 15.0, color: Colors.white,),
                  ),
                )),
            Padding(
              padding: const EdgeInsets.only(bottom: 16),
              child: SizedBox(
                width: 150,
                child: Text(item.calories.toString(),
                    style: new TextStyle(fontSize: 12.0, color: AppColors.getColor(COLOR_LIGHT_GRAY),)),
              ),
            )
          ]),
        ));
  }
}
