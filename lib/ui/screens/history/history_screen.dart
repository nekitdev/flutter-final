import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_final/assets/theme/app_colors.dart';
import 'package:flutter_app_final/data/model/firebase_model.dart';
import 'package:flutter_app_final/bloc/history/history_bloc.dart';

import 'history_item.dart';

class HistoryScreen extends StatelessWidget{

  @override
  Widget build(BuildContext context) => History();
}

class History extends StatefulWidget {

  @override
  State createState() => HistoryState();
}

class HistoryState extends State<History> {
  Widget _appBarTitle = Text("Flutter final", style: new TextStyle(color: Colors.white));
  Widget _sortButton = Icon(Icons.sort, color: Colors.white);
  bool _isNeedSortedFromMinToMax = true;
  List<Data> _dataList = List();
  HistoryManager historyManager = HistoryManager();

  @override
  void initState() {
    historyManager.getDataFromFireStore();
    super.initState();
  }

  @override
  void dispose() {
    historyManager.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
    backgroundColor: AppColors.getColor(COLOR_BACKGROUND),
    appBar: _buildAppBar(),
    body: _buildBody(),
  );

  AppBar _buildAppBar() {
    return AppBar(
        centerTitle: true,
        title: _appBarTitle,
        automaticallyImplyLeading: true,
        actions:<Widget>[
          IconButton(icon: _sortButton, onPressed: () {setState(() {_sortItems();});},),
        ]
    );
  }

  Widget _buildBody() {
    return StreamBuilder(
      stream: historyManager.getAllStream,
      builder: (context, AsyncSnapshot<List<Data>> snapshot){
        if(snapshot == null || !snapshot.hasData){
          return ListView();
        } else {
          _dataList = snapshot.data;
          return ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (BuildContext ctx, int index){
              return HistoryItem(item: _dataList[index]);
            },
          );
        }
      },
    );
  }

  void _sortItems() {
    setState(() {
      if (_isNeedSortedFromMinToMax){
        _dataList.sort((a, b) => a.calories.compareTo(b.calories));
        _isNeedSortedFromMinToMax = false;
      } else {
        _dataList.sort((b, a) => a.calories.compareTo(b.calories));
        _isNeedSortedFromMinToMax = true;
      }
    });
  }

}
