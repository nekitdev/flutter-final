import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class BaseWidget<bloc extends Bloc> extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      builder: (context) => getScreenBloc(),
      child: Stack(
        children: <Widget>[
          getWidget(context),
        ],
      ),
    );
  }

  bloc getScreenBloc();
  Widget getWidget(BuildContext context);
}