import 'package:flutter/material.dart';
import 'package:flutter_app_final/assets/theme/app_colors.dart';

class RecipeDetailScreen extends StatelessWidget {
  String image;
  List<String> ingredients;

  RecipeDetailScreen(
      {Key key, @required this.image, @required this.ingredients})
      : super(key: key);

  @override
  Widget build(BuildContext context) => RecipeDetail(image, ingredients);
}

class RecipeDetail extends StatefulWidget {
  String image;
  List<String> ingredients;

  RecipeDetail(String image, List<String> ingredients) {
    this.image = image;
    this.ingredients = ingredients;
  }

  @override
  State<StatefulWidget> createState() => RecipeDetailState(image, ingredients);
}

class RecipeDetailState extends State<RecipeDetail> {
  String image;
  List<String> ingredients;

  RecipeDetailState(image, ingredients) {
    this.image = image;
    this.ingredients = ingredients;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.getColor(COLOR_BACKGROUND),
      body: _bodyBuild()
    );
  }

  CustomScrollView _bodyBuild() {
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          pinned: true,
          floating: false,
          snap: false,
          title: Text("RecipeDetails",
              style: TextStyle(
                color: Colors.white,
                fontSize: 20.0,
              )),
          expandedHeight: 220.0,
          flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              background: Hero(
                  tag: image,
                  child: Image.network(image, fit: BoxFit.cover,)
              )
          ),
        ),
        SliverList(
            delegate: new SliverChildListDelegate(_buildList(ingredients))
        )
      ],
    );
  }

  List _buildList(List<String> ingredients) {
    List<Widget> listItems = List();

    for (int i = 0; i < ingredients.length; i++) {
      listItems.add(
          Padding(padding: EdgeInsets.all(10.0),
              child: Text(ingredients[i], style: TextStyle(fontSize: 15.0, color: Colors.white))
      ));
    }

    // only for show SliverAppBar functionality
    if (ingredients.length < 15){
      for (int i = 0; i < 15; i++) {
        listItems.add(
            Padding(padding: EdgeInsets.all(10.0),
                child: Text('item $i', style: TextStyle(fontSize: 15.0, color: Colors.white))
            ));
      }
    }

    return listItems;
  }
}
