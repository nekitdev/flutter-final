import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_app_final/ui/screens/recipe/recipe_screen.dart';

void main() => runApp(SplashScreen());

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) => MaterialApp(home: SplashStateful());
}


class SplashStateful extends StatefulWidget {
    @override
  _SplashStatefulState createState() => _SplashStatefulState();
}

class _SplashStatefulState extends State<SplashStateful> {

  @override
  void initState() {
    super.initState();
    _navigateToRecipeWithDelay(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image(image: AssetImage("assets/images/snup.gif"),)
          ],
        ),
      ),
    );
  }

  void _navigateToRecipeWithDelay(BuildContext context) async {
    Timer(const Duration(milliseconds: 2000), () {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => RecipeScreen()),
      );
    });
  }
}
