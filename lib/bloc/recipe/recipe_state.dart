import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_app_final/data/model/recipe_model.dart';

class RecipeState extends Equatable {
  RecipeState([List props = const []]) : super(props);
}

class InitialRecipeState extends RecipeState {}

@immutable
class ResultRecipeState extends RecipeState {
  final List<Hit> response;

  ResultRecipeState(this.response);
}

@immutable
class ErrorRecipeState extends RecipeState {
  final String message;

  ErrorRecipeState(this.message);
}

@immutable
class LoadingRecipeState extends RecipeState {}
