import 'package:equatable/equatable.dart';

class RecipeEvent extends Equatable {}

class RecipeActionEvent extends RecipeEvent {
  String recipe;

  RecipeActionEvent(this.recipe);
}
