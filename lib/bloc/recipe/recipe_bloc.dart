import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_app_final/bloc/recipe/recipe_event.dart';
import 'package:flutter_app_final/bloc/recipe/recipe_state.dart';
import 'package:flutter_app_final/data/model/recipe_model.dart';
import 'package:flutter_app_final/data/repositories/recipe/recipe_repository.dart';
import 'package:flutter_app_final/data/repositories/recipe/recipe_repository_impl.dart';
import 'package:flutter_app_final/utils/constants.dart';

class RecipeBloc extends Bloc<RecipeEvent, RecipeState> {
  RecipeRepository _recipeRepository;

  @override
  RecipeState get initialState {
    _recipeRepository = RecipeRepositoryImpl();
    return InitialRecipeState();
  }

  @override
  Stream<RecipeState> mapEventToState(RecipeEvent event) async* {
    if (event is RecipeActionEvent) {
      yield LoadingRecipeState();
      try {
        RecipeModel result = await _recipeRepository.getList(event.recipe);
        yield ResultRecipeState(result.hits);
      } catch (error) {
        yield error is ErrorRecipeState ? ErrorRecipeState(error.message) : ErrorRecipeState("ups");
      }
    }
  }

  void clearCloud(List<Hit> listHit){
    var ins = Firestore.instance.collection(RECIPES);
    ins.getDocuments().then((snapshot){
      for (DocumentSnapshot ds in snapshot.documents){
        ds.reference.delete();
      }
    }).then((_){
      _setDataToCloud(listHit, ins);
    });
  }

  void _setDataToCloud(List<Hit> listHit, CollectionReference ins) {
    for (var i = 0; i < listHit.length; i++){
      ins.document(i.toString()).setData({
        'image': listHit[i].recipe.image,
        'label': listHit[i].recipe.label,
        'calories': listHit[i].recipe.calories});
    }
  }


//  @override
//  Stream<HomeLatestState> mapEventToState(HomeLatestEvent event) async* {
//    yield LoadingLatestState();
//    if (event is LatestActionEvent) {
//      try {
//        LatestModelResponse result = await _latestRepository.getLatest(
//            event.page);
//        yield ResultLatestState(result.content);
//      } catch (error) {
//        yield error is SearchResultError
//            ? ErrorLatestState(error.message)
//            : ErrorLatestState("Упс... что-то пошло не так");
//      }
//    }
//  }
}
