import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_app_final/data/model/firebase_model.dart';
import 'package:flutter_app_final/utils/constants.dart';

class HistoryManager {

  final StreamController<List<Data>> _getAllData = StreamController.broadcast();
  Stream<List<Data>> get getAllStream => _getAllData.stream;


  void getDataFromFireStore() {
    List<Data> dataList = List();
    Firestore.instance.collection(RECIPES).getDocuments().then((snapshot){
      for (DocumentSnapshot ds in snapshot.documents){
        dataList.add(Data(label: ds.data['label'], image: ds.data['image'], calories: ds.data['calories']));
      }
      _getAllData.add(dataList);
    });
  }

  void dispose(){
    _getAllData?.close();
  }

}