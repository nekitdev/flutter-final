// To parse this JSON data, do
//
//     final recipeModel = recipeModelFromJson(jsonString);

import 'dart:convert';

RecipeModel recipeModelFromJson(String str) => RecipeModel.fromJson(json.decode(str));

String recipeModelToJson(RecipeModel data) => json.encode(data.toJson());

class RecipeModel {
  String q;
  int from;
  int to;
  Params params;
  bool more;
  int count;
  List<Hit> hits;

  RecipeModel({
    this.q,
    this.from,
    this.to,
    this.params,
    this.more,
    this.count,
    this.hits,
  });

  factory RecipeModel.fromJson(Map<String, dynamic> json) => RecipeModel(
    q: json["q"],
    from: json["from"],
    to: json["to"],
    params: Params.fromJson(json["params"]),
    more: json["more"],
    count: json["count"],
    hits: new List<Hit>.from(json["hits"].map((x) => Hit.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "q": q,
    "from": from,
    "to": to,
    "params": params.toJson(),
    "more": more,
    "count": count,
    "hits": new List<dynamic>.from(hits.map((x) => x.toJson())),
  };
}

class Hit {
  Recipe recipe;
  bool bookmarked;
  bool bought;

  Hit({
    this.recipe,
    this.bookmarked,
    this.bought,
  });

  factory Hit.fromJson(Map<String, dynamic> json) => new Hit(
    recipe: Recipe.fromJson(json["recipe"]),
    bookmarked: json["bookmarked"],
    bought: json["bought"],
  );

  Map<String, dynamic> toJson() => {
    "recipe": recipe.toJson(),
    "bookmarked": bookmarked,
    "bought": bought,
  };
}

class Recipe {
  String uri;
  String label;
  String image;
  String source;
  String url;
  String shareAs;
  double recipeYield;
  List<DietLabel> dietLabels;
  List<String> healthLabels;
  List<String> cautions;
  List<String> ingredientLines;
  List<Ingredient> ingredients;
  double calories;
  double totalWeight;
  Map<String, Total> totalNutrients;
  Map<String, Total> totalDaily;
  List<Digest> digest;

  Recipe({
    this.uri,
    this.label,
    this.image,
    this.source,
    this.url,
    this.shareAs,
    this.recipeYield,
    this.dietLabels,
    this.healthLabels,
    this.cautions,
    this.ingredientLines,
    this.ingredients,
    this.calories,
    this.totalWeight,
    this.totalNutrients,
    this.totalDaily,
    this.digest,
  });

  factory Recipe.fromJson(Map<String, dynamic> json) => Recipe(
    uri: json["uri"],
    label: json["label"],
    image: json["image"],
    source: json["source"],
    url: json["url"],
    shareAs: json["shareAs"],
    recipeYield: json["yield"],
    dietLabels: new List<DietLabel>.from(json["dietLabels"].map((x) => dietLabelValues.map[x])),
    healthLabels: new List<String>.from(json["healthLabels"].map((x) => x)),
    cautions: new List<String>.from(json["cautions"].map((x) => x)),
    ingredientLines: new List<String>.from(json["ingredientLines"].map((x) => x)),
    ingredients: new List<Ingredient>.from(json["ingredients"].map((x) => Ingredient.fromJson(x))),
    calories: json["calories"].toDouble(),
    totalWeight: json["totalWeight"].toDouble(),
    totalNutrients: Map.from(json["totalNutrients"]).map((k, v) => MapEntry<String, Total>(k, Total.fromJson(v))),
    totalDaily: Map.from(json["totalDaily"]).map((k, v) => MapEntry<String, Total>(k, Total.fromJson(v))),
    digest: new List<Digest>.from(json["digest"].map((x) => Digest.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "uri": uri,
    "label": label,
    "image": image,
    "source": source,
    "url": url,
    "shareAs": shareAs,
    "yield": recipeYield,
    "dietLabels": List<dynamic>.from(dietLabels.map((x) => dietLabelValues.reverse[x])),
    "healthLabels": List<dynamic>.from(healthLabels.map((x) => x)),
    "cautions": List<dynamic>.from(cautions.map((x) => x)),
    "ingredientLines": List<dynamic>.from(ingredientLines.map((x) => x)),
    "ingredients": List<dynamic>.from(ingredients.map((x) => x.toJson())),
    "calories": calories,
    "totalWeight": totalWeight,
    "totalNutrients": Map.from(totalNutrients).map((k, v) => MapEntry<String, dynamic>(k, v.toJson())),
    "totalDaily": Map.from(totalDaily).map((k, v) => MapEntry<String, dynamic>(k, v.toJson())),
    "digest": List<dynamic>.from(digest.map((x) => x.toJson())),
  };
}

enum DietLabel { LOW_CARB, HIGH_FIBER, BALANCED }

final dietLabelValues = EnumValues({
  "Balanced": DietLabel.BALANCED,
  "High-Fiber": DietLabel.HIGH_FIBER,
  "Low-Carb": DietLabel.LOW_CARB
});

class Digest {
  String label;
  String tag;
  SchemaOrgTag schemaOrgTag;
  double total;
  bool hasRdi;
  double daily;
  Unit unit;
  List<Digest> sub;

  Digest({
    this.label,
    this.tag,
    this.schemaOrgTag,
    this.total,
    this.hasRdi,
    this.daily,
    this.unit,
    this.sub,
  });

  factory Digest.fromJson(Map<String, dynamic> json) => Digest(
    label: json["label"],
    tag: json["tag"],
    schemaOrgTag: json["schemaOrgTag"] == null ? null : schemaOrgTagValues.map[json["schemaOrgTag"]],
    total: json["total"].toDouble(),
    hasRdi: json["hasRDI"],
    daily: json["daily"].toDouble(),
    unit: unitValues.map[json["unit"]],
    sub: json["sub"] == null ? null : List<Digest>.from(json["sub"].map((x) => Digest.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "label": label,
    "tag": tag,
    "schemaOrgTag": schemaOrgTag == null ? null : schemaOrgTagValues.reverse[schemaOrgTag],
    "total": total,
    "hasRDI": hasRdi,
    "daily": daily,
    "unit": unitValues.reverse[unit],
    "sub": sub == null ? null : List<dynamic>.from(sub.map((x) => x.toJson())),
  };
}

enum SchemaOrgTag { FAT_CONTENT, CARBOHYDRATE_CONTENT, PROTEIN_CONTENT, CHOLESTEROL_CONTENT, SODIUM_CONTENT, SATURATED_FAT_CONTENT, TRANS_FAT_CONTENT, FIBER_CONTENT, SUGAR_CONTENT }

final schemaOrgTagValues = EnumValues({
  "carbohydrateContent": SchemaOrgTag.CARBOHYDRATE_CONTENT,
  "cholesterolContent": SchemaOrgTag.CHOLESTEROL_CONTENT,
  "fatContent": SchemaOrgTag.FAT_CONTENT,
  "fiberContent": SchemaOrgTag.FIBER_CONTENT,
  "proteinContent": SchemaOrgTag.PROTEIN_CONTENT,
  "saturatedFatContent": SchemaOrgTag.SATURATED_FAT_CONTENT,
  "sodiumContent": SchemaOrgTag.SODIUM_CONTENT,
  "sugarContent": SchemaOrgTag.SUGAR_CONTENT,
  "transFatContent": SchemaOrgTag.TRANS_FAT_CONTENT
});

enum Unit { G, MG, UNIT_G, EMPTY, KCAL, IU }

final unitValues = EnumValues({
  "%": Unit.EMPTY,
  "g": Unit.G,
  "IU": Unit.IU,
  "kcal": Unit.KCAL,
  "mg": Unit.MG,
  "µg": Unit.UNIT_G
});

class Ingredient {
  String text;
  double quantity;
  Measure measure;
  String food;
  double weight;

  Ingredient({
    this.text,
    this.quantity,
    this.measure,
    this.food,
    this.weight,
  });

  factory Ingredient.fromJson(Map<String, dynamic> json) => Ingredient(
    text: json["text"],
    quantity: json["quantity"].toDouble(),
    measure: json["measure"] == null ? null : measureValues.map[json["measure"]],
    food: json["food"],
    weight: json["weight"].toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "text": text,
    "quantity": quantity,
    "measure": measure == null ? null : measureValues.reverse[measure],
    "food": food,
    "weight": weight,
  };
}

enum Measure { POUND, TABLESPOON, UNIT, CUP, DASH, STALK, CLOVE, TEASPOON, OUNCE, GRAM }

final measureValues = EnumValues({
  "clove": Measure.CLOVE,
  "cup": Measure.CUP,
  "dash": Measure.DASH,
  "gram": Measure.GRAM,
  "ounce": Measure.OUNCE,
  "pound": Measure.POUND,
  "stalk": Measure.STALK,
  "tablespoon": Measure.TABLESPOON,
  "teaspoon": Measure.TEASPOON,
  "<unit>": Measure.UNIT
});

class Total {
  String label;
  double quantity;
  Unit unit;

  Total({
    this.label,
    this.quantity,
    this.unit,
  });

  factory Total.fromJson(Map<String, dynamic> json) => Total(
    label: json["label"],
    quantity: json["quantity"].toDouble(),
    unit: unitValues.map[json["unit"]],
  );

  Map<String, dynamic> toJson() => {
    "label": label,
    "quantity": quantity,
    "unit": unitValues.reverse[unit],
  };
}

class Params {
  List<dynamic> sane;
  List<String> q;
  List<String> appKey;
  List<String> appId;

  Params({
    this.sane,
    this.q,
    this.appKey,
    this.appId,
  });

  factory Params.fromJson(Map<String, dynamic> json) => Params(
    sane: List<dynamic>.from(json["sane"].map((x) => x)),
    q: List<String>.from(json["q"].map((x) => x)),
    appKey: List<String>.from(json["app_key"].map((x) => x)),
    appId: List<String>.from(json["app_id"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "sane": List<dynamic>.from(sane.map((x) => x)),
    "q": List<dynamic>.from(q.map((x) => x)),
    "app_key": List<dynamic>.from(appKey.map((x) => x)),
    "app_id": List<dynamic>.from(appId.map((x) => x)),
  };
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
