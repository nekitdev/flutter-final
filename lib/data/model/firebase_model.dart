// To parse this JSON data, do
//
//     final firebaseModel = firebaseModelFromJson(jsonString);

import 'dart:convert';

FirebaseModel firebaseModelFromJson(String str) => FirebaseModel.fromJson(json.decode(str));

String firebaseModelToJson(FirebaseModel data) => json.encode(data.toJson());

class FirebaseModel {
  List<Document> documents;

  FirebaseModel({
    this.documents,
  });

  factory FirebaseModel.fromJson(Map<String, dynamic> json) => FirebaseModel(
    documents: List<Document>.from(json["documents"].map((x) => Document.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "documents": List<dynamic>.from(documents.map((x) => x.toJson())),
  };
}

class Document {
  Data data;

  Document({
    this.data,
  });

  factory Document.fromJson(Map<String, dynamic> json) => Document(
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "data": data.toJson(),
  };
}

class Data {
  String label;
  String image;
  double calories;

  Data({
    this.label,
    this.image,
    this.calories,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    label: json["label"],
    image: json["image"],
    calories: json["calories"].toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "label": label,
    "image": image,
    "calories": calories,
  };
}
