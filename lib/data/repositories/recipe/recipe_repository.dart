

import 'package:flutter_app_final/data/model/recipe_model.dart';

abstract class RecipeRepository {
  Future<RecipeModel> getList(String recipe);
}
