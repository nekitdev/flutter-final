import 'package:dio/dio.dart';
import 'package:flutter_app_final/data/model/recipe_model.dart';
import 'package:flutter_app_final/data/net/api/net_manager.dart';
import 'package:flutter_app_final/data/repositories/recipe/recipe_repository.dart';

class RecipeRepositoryImpl extends RecipeRepository {
  final NetManager client = NetManager();

  @override
  Future<RecipeModel> getList(String recipe) async {
    Response result = await client.getRecipes(recipe);
    return RecipeModel.fromJson(result.data);
  }
}
