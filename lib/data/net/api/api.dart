import 'package:dio/dio.dart';

abstract class API {
  Future<Response> getRecipes(String recipe);
}
