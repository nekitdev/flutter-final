import 'package:dio/dio.dart';
import 'package:flutter_app_final/data/net/api/api.dart';
import 'package:flutter_app_final/data/net/api/net_constants.dart';

class NetManager extends API {
  Map<String, String> _headersMap = <String, String>{
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  };

  static final NetManager _internal = NetManager.internal();

  factory NetManager() => _internal;

  NetManager.internal();

  Dio _getDioClient() {
    var options = new BaseOptions(baseUrl: BASE_URL, connectTimeout: 5000);
    Dio _dioClient = Dio(options)..options.headers.addAll(_headersMap);

    _dioClient.interceptors
        .add(LogInterceptor(requestBody: true, responseBody: true));
    return _dioClient;
  }

  // TODO need move to another package
  @override
  Future<Response> getRecipes(String recipe) async {
    Response response = await _getDioClient().get("/search?q=" + recipe + "&app_id=\$" + APP_ID + "&app_key=\$" + APP_KEY);
    return response;
  }
}
