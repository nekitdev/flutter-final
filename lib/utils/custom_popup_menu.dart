
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';


class CustomPopupMenu {
  String title;
  IconData icon;
  CustomPopupMenu({this.title, this.icon});
}

class SelectedOption extends StatelessWidget {
  CustomPopupMenu choice;

  SelectedOption({Key key, this.choice}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(choice.icon, size: 140.0, color: Colors.white),
            Text(choice.title, style: TextStyle(color: Colors.white, fontSize: 30))
          ],
        ),
      ),
    );
  }
}

enum MenuTitle { History, About }
